from django.shortcuts import render
from django.http import HttpResponse
from studentinfo.models import Studentdetails
from studentinfo.models import Coursedetails
from studentinfo.models import Enrollment
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def home(request):
	context={'name':'Hello World'}
	return render(request, 'studentinfo/home.html', context)

@login_required
def studentdetails(request):
	studentdata = Studentdetails.objects.all()
	paginator = Paginator(studentdata, 10)
	page = request.GET.get('page')
	minidata = paginator.get_page(page)
	context={'data':minidata}
	return render(request, 'studentinfo/studentdetails.html', context)

@login_required
def coursedetails(request):
	coursedata = Coursedetails.objects.all()
	paginator = Paginator(coursedata, 10)
	page = request.GET.get('page')
	minidata = paginator.get_page(page)
	context={'data':minidata}
	return render(request, 'studentinfo/coursedetails.html', context)

@login_required
def enrollment(request):
	studentdata = Studentdetails.objects.all()
	enrolldata = Enrollment.objects.all()
	context = {'student':studentdata, 'enroll':enrolldata}
	return render(request, 'studentinfo/enrollment.html', context)

def saveenrollment(request):
	if('sname' and 'cname' in request.GET):
		idname = request.GET.get('sname')
		coursename = request.GET.get('cname')
		dataobj = Enrollment(studentname = idname, coursetitle = coursename)