from django.db import models

# Create your models here.

class Studentdetails(models.Model):
	studentid = models.IntegerField(primary_key=True)
	firstname = models.CharField(max_length=500)
	lastname = models.CharField(max_length=500)
	major = models.CharField(max_length=500)
	year = models.CharField(max_length=500)
	gpa = models.CharField(max_length=500)

class Coursedetails(models.Model):
	courseid = models.IntegerField(primary_key=True)
	coursetitle = models.CharField(max_length=500)
	coursename = models.CharField(max_length=500)
	sectioncode = models.IntegerField()
	department = models.CharField(max_length=500)
	instructor = models.CharField(max_length=500)

class Enrollment(models.Model):
	studentname = models.IntegerField()
	coursetitle = models.CharField(max_length=500)